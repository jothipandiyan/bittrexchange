import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CurrencyPipe } from '@angular/common';
import { TableLayoutComponent } from './table-layout.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RecaptchaFormsModule } from 'ng-recaptcha/forms';
import { RecaptchaModule } from 'ng-recaptcha';
import { RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';
import {SlickModule} from 'ngx-slick'
import { MomentModule } from 'angular2-moment';
import { TableComponent } from 'app/shared/table/table.component';
import { OrderrByPipe } from 'app/shared/orderby.pipe';

@NgModule({
    imports: [ CommonModule,     RecaptchaModule.forRoot(),
        FormsModule,RouterModule,RecaptchaFormsModule ,SlickModule,MomentModule],
    declarations: [ 
        TableLayoutComponent, TableComponent,OrderrByPipe
    ],
    exports: [
        CommonModule, 
        TableLayoutComponent,
        TableComponent,
        OrderrByPipe,
        RecaptchaFormsModule,
        RecaptchaModule,
        SlickModule,
        MomentModule
    ],
    providers: [  {
        provide: RECAPTCHA_SETTINGS,
        useValue: { siteKey: '6LdbikAUAAAAAA5vMcCnk5ls86BC6HKTdRPq-DVw' } as RecaptchaSettings,
      }]
})
export class SharedModule { }