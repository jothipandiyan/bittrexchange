import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './index';
import { User } from '../models/index';
import { HttpHeaderResponse } from '@angular/common/http/src/response';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserService {

    isLogin;
    walletApi = 'api/wallet'
    constructor(
        private http: HttpClient,
        private authenticationService: AuthenticationService) {
    }

    
    getUsers(): Observable<User> {
        // get users from api
        return this.http.get('api/Users/GetUsers')
            .map((data:User) => data);
    }
 
    getWalletBalanceByCurrency(currency){
        return this.http.get(`${this.walletApi}/GetWalletBalCur?CurShort=${currency}`)
        .map((data:number) => data);
    }

    getOpenOrders(){         
        let url = `api/orders/GetOpenOrders`;
        return this.http.get(url)
    }
    
    getClosedOrders(){         
        let url = `api/orders/GetCompletedOrders`;
        return this.http.get(url)
    } 
    getTotalBalance(){         
        let url = `api/useraccount/gettotalbalance`;
        return this.http.get(url)
    }
         
    getOpenOrdersByMarket(market) {
        let url = `api/market/GetOpenOrdersMarket?currencyshortname=${market}`;
        return this.http.get(url)
    }
}