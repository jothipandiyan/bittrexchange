import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AdminService {
    currencyApiRoot = "api/Currencies"
    walletApiRoot = "api/Wallet"
    userApiRoot = "api/Users"

    constructor(private http: HttpClient) {
    }


    getCurrencyList() {
        let url = `${this.currencyApiRoot}/GetCurrencies`;
        // get users from api
        return this.http.get(url)
    }
    getCurrencyById(currencycode) {
        let url = `${this.currencyApiRoot}/GetCurrenciesbyId?currencycode=${currencycode}`;
        // get users from api
        return this.http.get(url)
            .map((response: Response) => response.json());
    }

    setCurrencyPrimary(currencycode) {
        let url = `${this.currencyApiRoot}/SetCurrenciesPrimary?currencyid=${currencycode}`;
        // get users from api
        return this.http.get(url)
    }

    getWalletBalance(walletguid):Observable<number> {
        let url = `api/wallet/GetWalletBal?wallet=${walletguid}`;
        let headers = new HttpHeaders();
        return this.http.get(url).map((data:number)=>data)
    }
    addBalance(data) {
        let url = `${this.walletApiRoot}/AddBalance`;

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')
        return this.http.post(url, data, { headers: headers })
    }
    addAmount(data) {
        let url = `${this.walletApiRoot}/AddAmount`;

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')
        return this.http.post(url, data, { headers: headers })
            .map((response: Response) => response.json())
    }
    getWalletsPerUser(accountId){

        let url = `api/UserAccount/GetAcWallets?Acid=${accountId}`;
        // get users from api
        return this.http.get(url)
    }
    createGenericWallet(data) {
        let url = `${this.walletApiRoot}/CreateGenericWallet`;

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')
        return this.http.post(url, data, { headers: headers })
    }

    createWallet(data) {
        let url = `${this.walletApiRoot}/CreateWallet`;

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')
        return this.http.post(url, data, { headers: headers })
            .map((response: Response) => response.json())
    }

    registerUser(user) {
        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')

        return this.http.post('api/Account/register', user, { headers: headers })
    }


    getOpenOrdersByAccountId(accountId){         
        let url = `api/orders/GetOpenOrdersUSer?Accountid=${accountId}`;
        return this.http.get(url)
    }
    getClosedOrdersByAccountId(accountId){         
        let url = `api/orders/GetCompletedOrdersUser?accountid=${accountId}`;
        return this.http.get(url)
    }

    getUserTotalBalance(accountId){
        let url = `api/useraccount/GetTotalBalanceUser?acid=${accountId}`;
        return this.http.get(url) 
    }
    getUsersByAccountId(accountId){
        let url = `api/Users/GetUsersbyAccountId?AccountId=${accountId}`;
        return this.http.get(url)

    }
    getUsers() {
        let url = `${this.userApiRoot}/GetUsers`;
        return this.http.get(url)
    }
    getUserTransactionHistory(accountId) {
        let url = `${this.userApiRoot}/GetUserTransHistory?Accountid=${accountId}`;
        return this.http.get(url)
    }
    getUserBuyHistory(accountId) {
        let url = `${this.userApiRoot}/GetUserBidHistory?Accountid=${accountId}`;
        return this.http.get(url)
    }
    getUserSellHistory(accountId) {
        let url = `${this.userApiRoot}/GetUserAskHistory?Accountid=${accountId}`;
        return this.http.get(url)
    }

    getLockedFunds(walletId){        
        let url = `api/wallet/GetLockedFunds?wallet=${walletId}`;
        return this.http.get(url)
    }
    
    getWalletList(){
        let url = `api/Wallet/GetWalletList`;
        return this.http.get(url)
    }

}