import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import {AuthenticationService} from '../services/index'
@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

    constructor(private router: Router,private authenticationService: AuthenticationService) { }

    canActivate() {
        console.log("canActivate")
        if (this.authenticationService.isAuthencticated()) {

            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }

    canActivateChild(){
        console.log("canActivateChild")
        if (this.authenticationService.isAuthencticated()) {
            // this.authenticationService.isLogin = true;
             // logged in so return true
             return true;
         }
 
         // not logged in so redirect to login page
//         this.router.navigate(['/market']);
         return false;
    }
}