import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common"
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
// Import containers
import {
  FullLayoutComponent
} from './containers';
import { NvD3Module } from 'ngx-nvd3';

const APP_CONTAINERS = [
  FullLayoutComponent
]

// Import components
import {
  AppAsideComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
} from './components';

const APP_COMPONENTS = [
  AppAsideComponent,
  AppFooterComponent,
  AppHeaderComponent,
  AppSidebarComponent,
  AppSidebarMinimizerComponent,
  APP_SIDEBAR_NAV
]

// Import directives
import {
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
} from './directives';

const APP_DIRECTIVES = [
  AsideToggleDirective,
  NAV_DROPDOWN_DIRECTIVES,
  ReplaceDirective,
  SIDEBAR_TOGGLE_DIRECTIVES
]

const Market_COMPONENTS = [
  BuyComponent,SellComponent,
  MarketComponent]
import { HttpModule } from '@angular/http';

// Import routing module
import { AppRoutingModule } from './app.routing';

//Import Service
import { AuthenticationService, UserService, AuthGuard, ApiService } from './services/index';
import { CookieService } from 'ngx-cookie-service';

// Import 3rd party components
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { ModalModule } from 'ngx-bootstrap/modal';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './interceptor/tokenInterceptor';
import {  MarketComponent, BuyComponent, SellComponent } from 'app/views/market';
import { SharedModule } from 'app/shared/shared.module';
import { PageNotFoundComponent } from 'app/shared/pageNotFound/not.found.component';
import { HomeComponent } from 'app/views/home/home.component';
@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    CommonModule,
    ModalModule,
    FormsModule,ReactiveFormsModule,
     HttpClientModule,
     SharedModule,
     NvD3Module
  ],
  declarations: [
    AppComponent,HomeComponent,
    ...APP_CONTAINERS,
    ...APP_COMPONENTS,
    ...APP_DIRECTIVES,
    Market_COMPONENTS,
     PageNotFoundComponent 
  ],
  providers: [
    AuthenticationService, 
    ApiService,PageNotFoundComponent,
    CookieService,
    AuthGuard,
    UserService,
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
