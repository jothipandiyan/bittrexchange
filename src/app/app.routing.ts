import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

// Import Containers
import {
  FullLayoutComponent
} from './containers';
import { MarketComponent } from 'app/views/market/index';
import { PageNotFoundComponent } from 'app/shared/pageNotFound/not.found.component';
import { HomeComponent } from 'app/views/home/home.component';

export const routes: Routes = [
  { path: '',  redirectTo:'markets', pathMatch: 'full' },
  
  { path: '',  component: FullLayoutComponent, data: {  title: 'Home' },
    children: [
              { path: 'login',  loadChildren: './views/login/login.module#LoginModule' },
              { path: 'register',  loadChildren: './views/register/register.module#RegisterModule' },

               { path: 'markets',  loadChildren: './views/markets/market.module#MarketModule' },
               { path: 'user',  loadChildren: './views/user/user.module#UserModule' },
               { path: 'admin',  loadChildren: './views/admin/admin.module#AdminModule' },
              {
                path: 'market', component: MarketComponent, data: { title: 'Market' }
                //, children: [
                //   {  path: '', canActivateChild: [AuthGuard],   children: [
            
                //   { path: '', component: BuySellComponent, canActivateChild: [AuthGuard] }] }
                // ]
              },           
              { path: '**', component: PageNotFoundComponent }
            
              ],

  }
  // { path: 'wallet',  data: {  title: 'Wallet' }, loadChildren: './views/components/user/user.module#UserModule' },
  
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
