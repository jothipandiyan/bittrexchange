import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarketsComponent } from './index';

import { AuthenticationService, AuthGuard } from 'app/services/index';

const routes: Routes = [
  { path: '', component: MarketsComponent, data: { title: 'Markets' },
  children:[
    {path: '/*', redirectTo:'/markets'}
  ] },
  {path: '/*', redirectTo:'/markets'}

]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MarketRoutingModule { }
