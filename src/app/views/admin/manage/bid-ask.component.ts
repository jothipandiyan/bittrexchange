import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'app/services';

@Component({
  selector: 'bid-ask',
  templateUrl: './bid-ask.component.html'
})
export class BidAskComponent implements OnInit {
  

  buySellheaderSettings = [{
    "visible": false,
    "fieldName": "secondcurid",
    "displayName": "secondcurid"
  }, {
    "visible": true,
    "fieldName": "units",
    "displayName": "Units"
  }, {
    "visible": true,
    "fieldName": "Total",
    "displayName": "Total"  
  }, {
    "visible": true,
    "fieldName": "rate",
    "displayName": "Rate"
  }, {
    "visible": true,
    "fieldName": "sum",
    "displayName": "SUM"
  }]
  accountId
  queryParamSubscriber$
  constructor( private router: Router,private activatedRoute:ActivatedRoute, private admin:AdminService) { }

  ngOnInit(): void {
    this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(params => {
      this.accountId = params['accountId'] || null;
      this.getUserAsks(this.accountId)
      this.getUserBids(this.accountId)

    });
  }
  buyHistory
  askHistory
  ngOnDestroy() {
    if(this.queryParamSubscriber$)
      this.queryParamSubscriber$.unsubscribe();
  }
  getUserBids(accountId){
    this.admin.getUserBuyHistory(accountId).subscribe(res=>{
      if(Array.isArray(res))  
        this.buyHistory = res
    })
  }
  getUserAsks(accountId){
    this.admin.getUserSellHistory(accountId).subscribe(res=>{
      if(Array.isArray(res))  
        this.askHistory = res
    })

  }

}
