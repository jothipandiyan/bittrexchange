import { Component } from '@angular/core';
import { AuthenticationService } from 'app/services';

@Component({
  selector: 'app-sidebar',
  templateUrl: './app-sidebar.component.html'
})
export class AppSidebarComponent {

  constructor(private authenticationService:AuthenticationService){
    
  }
 }
